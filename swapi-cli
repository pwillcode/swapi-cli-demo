#!/usr/local/bin/python3
import swapi
from clint.textui import prompt, colored
import re

SPLASH = """     _______.___________.    ___      .______         ____    __    ____  ___      .______          _______.
    /       |           |   /   \     |   _  \        \   \  /  \  /   / /   \     |   _  \        /       |
   |   (----`---|  |----`  /  ^  \    |  |_)  |        \   \/    \/   / /  ^  \    |  |_)  |      |   (----`
    \   \       |  |      /  /_\  \   |      /          \            / /  /_\  \   |      /        \   \    
.----)   |      |  |     /  _____  \  |  |\  \----.      \    /\    / /  _____  \  |  |\  \----.----)   |   
|_______/       |__|    /__/     \__\ | _| `._____|       \__/  \__/ /__/     \__\ | _| `._____|_______/   

                                    _   ___ ___   ___  ___ __  __  ___  
                                   /_\ | _ \_ _| |   \| __|  \/  |/ _ \ 
                                  / _ \|  _/| |  | |) | _|| |\/| | (_) |
                                 /_/ \_\_| |___| |___/|___|_|  |_|\___/"""
ANSI_CLEAR = chr(27) + '[2J'
SWAPI_OBJECT_TYPE_CONFIG = {
  'people': {'get_function': 'get_person', 'list_header': 'People', 'item_header': 'Person'},
  'vehicles': {'get_function': 'get_vehicle', 'list_header': 'Vehicles', 'item_header': 'Vehicle'},
  'films': {'get_function': 'get_film', 'list_header': 'Films', 'item_header': 'Film'},
  'starships': {'get_function': 'get_starship', 'list_header': 'Starships', 'item_header': 'Starship'},
  'species': {'get_function': 'get_species', 'list_header': 'Species', 'item_header': 'Species'},
  'planets': {'get_function': 'get_planet', 'list_header': 'Planets', 'item_header': 'Planet'}
}
SWAPI_OBJECT_TYPE_LIST = '({})'.format(', '.join(SWAPI_OBJECT_TYPE_CONFIG.keys()))
PROMPTS = {
  'main': '\n'.join([
    ANSI_CLEAR,
    str(colored.yellow(SPLASH)),
    '\n',
    'Which type of entry would you like to list?',
    SWAPI_OBJECT_TYPE_LIST
  ]),
  'object': '\n'.join(['Enter an item number or another object type.', SWAPI_OBJECT_TYPE_LIST])
}

def print_object_list_summary(swapi_object, header):
  '''Prints a summary of a query set as returned by the SWAPI API wrapper

  Attributes:
    swapi_object(swapi.models.BaseQuerySet): The object to print.
    header(string): The header string to introduce the object as.
  '''
  print(render_header(header))
  items = sorted(swapi_object.items, key=(lambda item: int(get_id_from_object_url(item.url))))
  print('\n'.join(map((lambda item: render_short(item)), items)))

def print_object_summary(swapi_object, header):
  '''Prints a summary of a single object as returned by the SWAPI API wrapper

  Attributes:
    swapi_object(swapi.models.BaseModel): The object to print.
    header(string): The header string to introduce the object as.
  '''
  print(render_header(header))
  print(render_long(swapi_object))

def render_header(title):
  return '{:~^30}'.format(title)

def render_long(swapi_object):
  attributes_to_render = [a for a in dir(swapi_object) if not a.startswith('__') and not callable(getattr(swapi_object, a)) and not a == 'url']
  attributes_to_render = sorted(attributes_to_render, key=(lambda name: '_first_' if name == 'name' or name == 'title' else name))
  return '\n'.join(map((lambda item: render_attribute(item, swapi_object)), attributes_to_render))

def render_short(swapi_object):
  prefix = '{:3}'.format('{}:'.format(get_id_from_object_url(swapi_object.url)))
  return '{} {}'.format(prefix, get_swapi_object_name(swapi_object))

def get_swapi_object_name(swapi_object):
  '''Gets an appropriate display name (ex: "Yoda") of a SWAPI object.

  Attributes:
    swapi_object(swapi.models.BaseModel): The object to print.

  Returns:
    string: The name of the object.
  '''
  return swapi_object.title if isinstance(swapi_object, swapi.models.Film) else swapi_object.name if hasattr(swapi_object, 'name') else render_unsupported(swapi_object)

def get_id_from_object_url(url):
  '''Gets the ID from a SWAPI object URL.

  Attributes:
    url(string): The canonical url of a single SWAPI object.

  Returns:
    string: The string ID of the object
  '''
  return re.search('/(\d+)/?$', url).group(1)

def render_unsupported(swapi_object):
  return '{} (unsupported SWAPI object type)'.format(swapi_object)

def render_attribute(attribute, swapi_object):
  '''Renders an attribute on a SWAPI object.

  Attributes:
    attribute(string): The name of the attribute to render.
    swapi_object(swapi.models.BaseModel): The object containing the attribute.

  Returns:
    string: The rendered attribute
  '''
  value = getattr(swapi_object, attribute)
  if isinstance(value, list):
    prefix = '#s ' if len(value) > 1 else '# ' if len(value) == 1 else 'None'
    value = '{}{}'.format(prefix , ', '.join(map((lambda item: get_id_from_object_url(item)), value)))
  return '{}: {}'.format(attribute, value)

def main():
  context = 'main'
  object_type = None
  while True:
    choice = prompt.query('\n\n' + PROMPTS[context])
    print('\n')
    if choice in SWAPI_OBJECT_TYPE_CONFIG:
      context = 'object'
      object_type = choice
      print_object_list_summary(swapi.get_all(choice), SWAPI_OBJECT_TYPE_CONFIG[choice]['list_header'])
    elif object_type != None and choice.isdigit():
      context = 'object'
      config = SWAPI_OBJECT_TYPE_CONFIG[object_type]
      print_object_summary(getattr(swapi, config['get_function'])(int(choice)), config['item_header'])

if __name__ == "__main__":
    main()
